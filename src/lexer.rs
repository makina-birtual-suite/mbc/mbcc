enum Ops {
    Plus,
    Minus,
    Mul,
    Div,
    Ref,
    MutRef,
    Deref,
    DerefL,
    Defer,
}

enum Val {
    Integer(String),
    Float(String),
    Stryng(String),
    Bool(String),
    Ptr(Box<Val>),
    MutPtr(Box<Val>),
    Array(Vec<Val>),
}

enum TokenKind {
    Operator {
        op      : Ops,
        right   : Box<TokenKind>,
        left    : Box<TokenKind>,
    },
    
}


struct Token {
    loc : (u64, u64),
    func: TokenKind,
}

# MBC compiler (makina birtual's simple c-like language compiler)

MBC is a C-like/Ml-like simple language.

## Design principles:

### Operators
Can't be overloaded.
- '+'                           (addition)          a (+) a :: a -> a -> a 
- '-'                           (subtraction)       a (-) a :: a -> a -> a
- '/'                           (division) 
- '*'                           (multiplication)
- '|'                           (bitwise or)
- '&'                           (bitwise and)
- '<<' '>>'                     (bitshifts)
- '<' '>' '>=' '<=' '==' '!='   (comparison)
- '+= -= *= /= |= &='           (composite assignment)
- 'ref'                         (refer)                         (ref) a :: a -> Ptr a
- 'mutref'                      (refer mutably)                 (mutref) a :: a -> MutPtr a
- 'deref'                       (dereference)                   (deref) T a ; (T <- Ptr | MutPtr | Ptr[] | MutPtr[]) :: T a -> (RawPtr a) 
- 'deref[]'                     (dereference with indexing)     (deref[]) (T a , u64) ; (T <- Ptr[] | MutPtr[]) :: T a -> u64 -> (RawPtr a)
- 'sizeof'                      (size of allocation in bytes)   (sizeof) a :: a -> u64 | T a -> u64  
- 'defer!'                      (defers an action to the end of the scope)

### Keywords
- return  (returns)
- halt    (stops the program)

### Simple intrinsic type system
The type system will include very simple types and the RawPtr, Ptr, MutPtr, Ptr[], MutPtr[] "kinds":
- Unsigned integers : u8, u16, u32, u64 
- Signed integers   : i8, i16, i32, i64
- Booleans (true and false)
- Structs
- Enums (as proper sum types)
- Tuples (anonymous structs)
It will not include string as a type, but string literals will exist as Ptr[] u8 and MutPtr[] u8.

RawPtr kind should not be used! They are temporary values and not guaranteed to be valid after ANY allocation. 
They should ONLY EXIST in the context of the left side of an assignment. 
Since RawPtr are absolute adresses, they point ONLY to single elements, therefore only RawPtr, and not RawPtr[], exists.
RawPtr is the basic type of a variable, therefore it auto defers.

It will have comptime generics.

It will have type inference.

Since this runs on Makina, all Ptr kinds know their allocation size and are fat pointers.
Using this, we can guarantee safety.

The basic syntax of function declaration will be: 

```rust 
let id = fn (u64 -> u64) {
    where a:
        return a;
    where 0:
        return 0;         
}  

```
To get more than one argument, you can use currying: 
```rust
let foo = fn (u64 -> u64 -> u64) {
    where a,b:
        return a + b;
    where 0,b:
        return b; 
}
```
Which implicitly creates an anonymous funcion when partially called.

For generic functions, we declare the comptime type before the arguments in the type signature block, as such: 
```rust
let bar = fn (comptime T, T -> T -> T ) {
    where a,b;
        return a + b;
}
```
There is no special syntax for pointers. Instead, we rely on the *deref*, *mutref* and the *ref* operators, and the *size* operator:

Note: you can either have multiple Ptr to a value OR a single MutPtr.

### Example
```rust

let IceCream = enum (Mint, StrawBerry, Caramel, Chocolate) {}

let Option = enum (comptime T, Some T, None) {
    let unwrap = fn (self -> T) { 
        // self != Self. self implies a method, while Self implies a function that takes an object of the same type.
        where Some x: 
            return x;
        where None:
            println("PANIC: Tried to Unwrap a None");
            exit(1);
    }
}


let View = struct (comptime T, u64, u64, Ptr[] T) 
    where Self (offset, end, contents) {
    let new = fn (u64 -> u64 -> Ptr[] T -> View T) {
        where offs, nd, cont:
            let res = Self (offs, nd, cont);
            return res; 
    }

    let free = fn (self) {
        where self:
            free(self);
        // we do not free the contents as this is a slice, and we do not own the data.
    }

    let at = fn (ref self -> u64 -> Option T) {
        where self, offs if offs < (self.end - self.offset):  
            return Option.Some (deref[self.offset + offs] self.contents);
        where _:
            return Option.None;
    }
};

let main = fn () {
    where: 
        mut   a : i32 = 10; // a : i32 //
        let b = mutref a; // b : MutPtr i32 //
        deref b += 10;
        let c : i32 = 34;
        let b = ref c; // shadows b, b : Ptr i32 //
        deref b += 10; // Compile time error! Cannot assign to the contents of b as it is a immutable reference. //
        // B will be ignored for the following example //
        let d = mutref c; // Compile time error! c is a constant and therefore you cannot obtain a mutable reference to it, but also you can't assign a mutable reference to a constant //
        // Ignoring above code //
        mut arr : MutPtr[] u64 = malloc u64 9; // Variables that contain MutPtr or MutPtr[] must be marked with mut. Malloc automatically initializes memory to 0
        defer! free(arr);
        for i (0, sizeof arr / sizeof u64) {
            // There is no right way to access an array. Here, we rely on pointer arithmetic to access and modify data //
            deref (arr + i * (sizeof u64)) = i + 1;  
        }
        for i (0, sizeof arr / sizeof u64) {
            // Alternatively, we can use the deref[] operator to access arrays //
            deref[i] arr *= 2;            
        }
    
        const window = View.new 1 5 arr; // View u64

        let favourite_ice_cream = Ice_Cream.Mint;
        fn (IceCream -> ()) {
            where IceCream.Mint:
                println("Great taste!");
            where _:
                println("MEH!");
        } favorite_ice_cream;
        println("Number at : {0}", (window.at 1).unwrap);
}

```

